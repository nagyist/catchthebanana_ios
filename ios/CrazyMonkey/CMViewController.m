//
//  CMViewController.m
//  CrazyMonkey
//
//  Created by Aurindam Jana on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CMViewController.h"

@interface CMViewController()
- (void)onTransitionToInactive: (NSNotification *)sender;
- (void)onTransitionToActive: (NSNotification *)sender;
@end

@implementation CMViewController

@synthesize stage;

- (id)initWithFrame:(CGRect)frame
{
    self = [super init];
    if (self) {
        m_sparrowView = [[SPView alloc] initWithFrame:frame];
        m_sparrowView.clipsToBounds = YES;
        m_sparrowView.multipleTouchEnabled = YES;   
    }
    return self;
}

#pragma mark -
#pragma mark View Life Cycle

// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
	[super loadView];
    
//	[[self view] addSubview: m_sparrowView];
    [self setView:m_sparrowView];
    [m_sparrowView release];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onTransitionToInactive:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onTransitionToActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
	self.stage = nil;
}

#pragma mark -
#pragma mark Rotation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark -
#pragma mark Memory Managment

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)dealloc
{
    [self.stage release];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    
    [super dealloc];
}

#pragma mark -
#pragma mark Accessor Methods

- (void)setStage:(SPStage *)aStage {
	if (aStage) {
		SPView *spView = (SPView *)[self view];
        
		spView.stage = aStage;
		spView.frameRate = 30.0;
		[spView start];
        
		[SPAudioEngine start:SPAudioSessionCategory_AmbientSound];
	}
}

#pragma mark -
#pragma mark Notifications

- (void)onTransitionToActive:(NSNotification *)sender {
    SPView *spView = (SPView *)[self view];
    [spView start];
    
    [SPAudioEngine start];
}

- (void)onTransitionToInactive:(NSNotification *)sender {
    SPView *spView = (SPView *)[self view];
    [spView stop];
    
    [SPAudioEngine stop];
}

@end
