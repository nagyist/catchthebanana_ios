//
//  CMAppDelegate.h
//  CrazyMonkey
//
//  Created by Aurindam Jana on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMViewController.h"

@interface CMAppDelegate : UIResponder <UIApplicationDelegate>
{
    UIWindow *m_window;
    CMViewController *m_viewController;
}

@end
