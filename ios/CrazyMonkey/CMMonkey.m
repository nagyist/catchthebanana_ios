//
//  CMMonkey.m
//  CrazyMonkey
//
//  Created by Aurindam Jana on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CMMonkey.h"
#import "CMConstants.h"

@interface CMMonkey ()
- (void)onTouch:(SPTouchEvent*)event;
- (void)onMoveUpCompleted:(SPEvent*)event;
- (void)onMoveDown1Completed:(SPEvent*)event;
- (void)onMoveDown2Completed:(SPEvent*)event;
- (void)onHittingCoconut:(NSNotification*)aNotification;
- (void)onFallDownCompleted:(SPEvent*)event;
@end

@implementation CMMonkey

- (id)initWithScreenWidth:(float)width height:(float)height
{
    self = [super init];
    if (self) {
        m_juggler = [[SPJuggler alloc] init];
        m_walkingMonkey = [[SPTexture alloc] initWithContentsOfFile:@"walkingmonk.png"];
        m_sittingMonkey = [[SPTexture alloc] initWithContentsOfFile:@"sittingmonk.png"];
        m_jumpingMonkey = [[SPTexture alloc] initWithContentsOfFile:@"jumpmonk.png"];
        
        m_monkey = [[SPImage alloc] init];
        
        [m_monkey setTexture:m_walkingMonkey];
        [m_monkey setHeight:m_walkingMonkey.height];
        [m_monkey setWidth:m_walkingMonkey.width];
        
        [m_monkey setX:(width + m_monkey.width)/2];
        [m_monkey setY:height - m_monkey.height - GROUND_Y_POSITION];        
        
        m_offsetX = 0;
        m_offsetY = 0;
        m_screenWidth = width;
        m_screenHeight = height;
        
        m_isGrabbed = NO;
        m_isTouchEnabled = YES;
        m_isHit = NO;
        
        [self addChild:m_monkey];
        [self addEventListener:@selector(onTouch:) atObject:self forType:SP_EVENT_TYPE_TOUCH];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onHittingCoconut:) name:MONKEY_HIT_COCONUT object:nil];
    }
    return self;
}

#pragma mark -
#pragma mark Memory Managment

- (void)dealloc
{
    [m_juggler release];
    [m_walkingMonkey release];
    [m_sittingMonkey release];
    [m_jumpingMonkey release];
    [m_monkey release];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MONKEY_HIT_COCONUT object:nil];
    [super dealloc];
}

#pragma mark -
#pragma mark Notifications

- (void)onTouch:(SPTouchEvent*)event
{
    if (!m_isTouchEnabled || m_isHit)
        return;
    
    SPTouch *touch = [[event touchesWithTarget:m_monkey andPhase:SPTouchPhaseBegan] anyObject];
	SPTouch *drag = [[event touchesWithTarget:self andPhase:SPTouchPhaseMoved] anyObject];
	SPTouch *touchUp = [[event touchesWithTarget:m_monkey andPhase:SPTouchPhaseEnded] anyObject];
    
	if (touch) {
		SPPoint *touchPosition = [touch locationInSpace:m_monkey];
		m_isGrabbed = YES;
		m_offsetX = touchPosition.x;
		m_offsetY = touchPosition.y;
	} else if (drag) {
		if (m_isGrabbed) {
			SPPoint *dragPosition = [drag locationInSpace:self];
            float positionX = dragPosition.x - m_offsetX;
            if (positionX > 0 && positionX < m_screenWidth - m_monkey.width) {
                m_monkey.x = dragPosition.x - m_offsetX;
                [[NSNotificationCenter defaultCenter] postNotificationName:MONKEY_X_POSITION_CHANGED object:self];
            }
		}
	} else if (touchUp) {
		m_isGrabbed = NO;
        m_isTouchEnabled = NO;
        
        [m_monkey setTexture:m_jumpingMonkey];
        SPTween *tween = [SPTween tweenWithTarget:m_monkey time:0.5 transition:SP_TRANSITION_EASE_OUT];
        [tween animateProperty:@"y" targetValue:CEILING_Y_POSITION];
        [tween addEventListener:@selector(onMoveUpCompleted:) atObject:self forType:SP_EVENT_TYPE_TWEEN_COMPLETED];
        [m_juggler addObject:tween];
	}
}

- (void)onMoveUpCompleted:(SPEvent*)event
{
    SPTween *tween = [SPTween tweenWithTarget:m_monkey time:0.4 transition:SP_TRANSITION_EASE_IN];
    [tween animateProperty:@"y" targetValue:(m_screenHeight - m_monkey.height)/2];
    tween.delay = 0.1;
    [tween addEventListener:@selector(onMoveDown1Completed:) atObject:self forType:SP_EVENT_TYPE_TWEEN_COMPLETED];
    [m_juggler addObject:tween];
}

- (void)onMoveDown1Completed:(SPEvent*)event
{
    SPTween *tween = [SPTween tweenWithTarget:m_monkey time:0.4 transition:SP_TRANSITION_EASE_OUT_BOUNCE];
    [tween animateProperty:@"y" targetValue:m_screenHeight - m_monkey.height - GROUND_Y_POSITION];
    [tween addEventListener:@selector(onMoveDown2Completed:) atObject:self forType:SP_EVENT_TYPE_TWEEN_COMPLETED];
    [m_juggler addObject:tween];
}

- (void)onMoveDown2Completed:(SPEvent*)event
{
    [m_monkey setTexture:m_walkingMonkey];
    m_isTouchEnabled = YES;
    m_isHit = NO;
}

- (void)onHittingCoconut:(NSNotification*)aNotification
{
    if (m_isHit)
        return;
    
    m_isHit = YES;
    
    [m_juggler removeAllObjects];
    
    [m_monkey setTexture:m_sittingMonkey];
    SPTween *tween = [SPTween tweenWithTarget:m_monkey time:1.4 transition:SP_TRANSITION_EASE_OUT_BOUNCE];
    [tween animateProperty:@"y" targetValue:m_screenHeight - m_monkey.height - GROUND_Y_POSITION];
    [tween addEventListener:@selector(onFallDownCompleted:) atObject:self forType:SP_EVENT_TYPE_TWEEN_COMPLETED];
    [m_juggler addObject:tween];
}

- (void)onFallDownCompleted:(SPEvent*)event
{
    m_isHit = NO;
    [[m_juggler delayInvocationAtTarget:self byTime:2] onMoveDown2Completed:nil];
}

#pragma mark -
#pragma mark Accessors

- (float)monkeyX
{
    return m_monkey.x;
}

- (float)monkeyY
{
    return m_monkey.y;
}

- (float)monkeyWidth
{
    return m_monkey.width;
}

- (float)monkeyHeight
{
    return m_monkey.height;
}

- (void)advanceTime:(double)seconds
{
//    NSLog(@"Time passed since last frame: %f", seconds);
    if (!m_isTouchEnabled || m_isHit) {
        [m_juggler advanceTime:seconds];
        [[NSNotificationCenter defaultCenter] postNotificationName:MONKEY_Y_POSITION_CHANGED object:self];
    }
}

- (bool)dazed
{
    return m_isHit;
}

@end
