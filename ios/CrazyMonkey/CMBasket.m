//
//  CMBasket.m
//  CrazyMonkey
//
//  Created by Aurindam Jana on 6/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CMBasket.h"
#import "CMConstants.h"

@interface CMBasket ()
- (void)onAddBanana:(NSNotification*)aNotification;
@end

@implementation CMBasket

- (id)initWithScreenWidth:(float)width height:(float)height
{
    self = [super init];
    if (self) {
//        m_score = [[SPTextField alloc] initWithText:@"Score"];
//        [m_score setX:0];
//        [m_score setY:height - m_score.height - GROUND_Y_POSITION];
//        [m_score setFontName:@"Helvetica"];
//        [m_score setFontSize:32.0f];
//        [m_score setColor:0xffffff];
//        [m_score setHAlign:SPHAlignLeft];
        
        m_screenHeight = height;
        
        m_basket = [[SPImage alloc] initWithContentsOfFile:@"basket.png"];
        [m_basket setX:0];
        [m_basket setY:height - m_basket.height - GROUND_Y_POSITION];
        
        m_bananas = [[NSMutableArray alloc] initWithCapacity:MAX_BANANA_SCORE];
        
        [self addChild:m_basket];
//        [self addChild:m_score];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAddBanana:) name:ADD_BANANA_TO_BASKET object:nil];
    }
    return self;
}

#pragma mark -
#pragma mark Memory Managment

- (void)dealloc
{
    [m_basket release];
//    [m_score release];
    [m_bananas release];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ADD_BANANA_TO_BASKET object:nil];
    [super dealloc];
}

#pragma mark -
#pragma mark Accessors

- (float)top
{
    return m_basket.y;
}

- (void)setBanana:(SPTexture*)banana
{
    m_bananaTexture = banana;
}

- (NSUInteger)score
{
    return [m_bananas count];
}

#pragma mark -
#pragma mark Notifications

- (void)onAddBanana:(NSNotification*)aNotification
{
    if (m_bananas.count >= MAX_BANANA_SCORE)
        return;

    SPImage *banana = [SPImage imageWithTexture:m_bananaTexture];
    [banana setPivotX:banana.width/2];
    [banana setPivotY:banana.height/2];
    [banana setRotation:SP_D2R(90)];
    [banana setX:banana.width/2];
    [banana setY:m_screenHeight - m_basket.height - GROUND_Y_POSITION - (m_bananas.count + 1)*banana.height];
    
    [m_bananas addObject:banana];
    
    [self addChild:banana];
}

@end
